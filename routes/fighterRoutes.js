const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();
const fighterHandler = require("./handlers/fighter");

// console.log(fighterHandler.getOne);

// TODO: Implement route controllers for fighter
router
  .get("/",fighterHandler.getAll, responseMiddleware)
  .get("/:id", fighterHandler.getOne, responseMiddleware)
  .post("/", createFighterValid, fighterHandler.createFighter, responseMiddleware)
  .put("/:id", updateFighterValid, fighterHandler.updateFighter, responseMiddleware)
  .delete("/:id", fighterHandler.deleteFighter, responseMiddleware);

module.exports = router;
