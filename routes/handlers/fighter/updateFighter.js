const FighterService = require("../../../services/FighterService");

const updateFighter = (req, res, next) => {
  let fighterId = req.params.id;
  let fighter = res.updatedFighter;
  let updatedFighter = FighterService.update({ id: fighterId, dataToUpdate: fighter })
    .then((fighter) => {
      res.data = fighter;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "User entity to create is not valid",
      };
      next();
    });
};

module.exports = { updateFighter };