const FighterService = require("../../../services/FighterService");

const getOne = (req, res, next) => {
  try {
    const fighterId = req.params.id;
    let fighter = FighterService.getOne(fighterId);
    if (fighter === null) {
      res.data = { fighter: 'Fighter not found' };
      next();
    }
    res.data = { fighter: fighter };
    next();
  } catch (error) {
    if (error) {
      res.error = {
        error: true,
        message: "Fighter not found",
      };
      next();
    }
  }
};

module.exports = { getOne };
