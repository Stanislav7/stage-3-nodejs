const FighterService = require("../../../services/FighterService");

const getAll = (req, res, next) => {
  try {
    const items = FighterService.getAll();
    res.data = { fighter: items };
  } catch (error) {
    if (error) {
      res.error = {
        error: true,
        message: "Fighters not found",
      };
    }
  }
  next();
};

module.exports = { getAll };
