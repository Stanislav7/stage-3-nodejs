const { getAll } = require("./getAll.js");
const { getOne } = require("./getOne.js");
const { createFighter } = require("./createFighter.js");
const { updateFighter } = require("./updateFighter.js");
const { deleteFighter } = require("./deleteFighter.js");

module.exports = { getAll, getOne,  createFighter, updateFighter, deleteFighter};
