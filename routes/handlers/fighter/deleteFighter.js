const FighterService = require("../../../services/FighterService");

const deleteFighter = (req, res, next) => {
  let fighterId = req.params.id;
  let updatedFighter = FighterService.delete(fighterId)
    .then((fighter) => {
      res.data = fighter;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "User entity to create is not valid",
      };
      next();
    });
};

module.exports = { deleteFighter };