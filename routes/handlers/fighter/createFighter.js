const FighterService = require("../../../services/FighterService");

const createFighter = (req, res, next) => {
  let fighter = res.updatedFighter;
  let newFighter = FighterService.create(fighter)
    .then((fighter) => {
      res.data = fighter;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "Fighter entity to create is not valid",
      };
      next();
    });
};

module.exports = { createFighter };
