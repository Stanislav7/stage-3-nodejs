const { getAll } = require("./getAll.js");
const { getOne } = require("./getOne.js");
const { createUser } = require("./createUser.js");
const { updateUser } = require("./updateUser.js");
const { deleteUser } = require("./deleteUser.js");


module.exports = { getAll, getOne, createUser, updateUser, deleteUser };
