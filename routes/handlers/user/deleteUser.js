const UserService = require("../../../services/userService");

const deleteUser = (req, res, next) => {
  let userId = req.params.id;
  let updatedUser = UserService.delete(userId)
    .then((user) => {
      res.data = user;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "User entity to create is not valid",
      };
      next();
    });
};

module.exports = { deleteUser };

