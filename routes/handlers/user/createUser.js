const UserService = require("../../../services/userService");

const createUser = (req, res, next) => {
  let user = res.newUser;
  let newUser = UserService.create(user)
    .then((user) => {
      res.data = user;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "User entity to create is not valid",
      };
      next();
    });
};

module.exports = { createUser };

/*
id: '',
    firstName: 'test',
    lastName: 'test',
    email: 'test@test.com',
    phoneNumber: '123',
    password: '123' // min 3 symbols

*/
