const UserService = require("../../../services/userService");

const getOne = (req, res, next) => {
  try {
    const userId = req.params.id;

    let user = UserService.getOne(userId);

    if (user === null) {
      res.data = { user: "User not found" };
      next();
    }
    res.data = { user: user };

    next();
  } catch (error) {
    if (error) {
      res.error = {
        error: true,
        message: "User not found",
      };
      next();
    }
  }
};

module.exports = { getOne };
