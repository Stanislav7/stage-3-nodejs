const UserService = require("../../../services/userService");

const getAll = (req, res, next) => {
  try {
    const items = UserService.getAll();
    res.data = { users: items };
  } catch (error) {
    if (error) {
      res.error = {
        error: true,
        message: "Users not found",
      };
    }
  }
  next();
};

module.exports = { getAll };
