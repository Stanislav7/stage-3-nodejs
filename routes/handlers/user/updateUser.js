const UserService = require("../../../services/userService");

const updateUser = (req, res, next) => {
  let userId = req.params.id;
  let user = res.updatedUser;
  let updatedUser = UserService.update({ id: userId, dataToUpdate: user })
    .then((user) => {
      res.data = user;
    })
    .then(() => next())
    .catch((error) => {
      res.error = {
        error: true,
        message: "User entity to create is not valid",
      };
      next();
    });
};

module.exports = { updateUser };
