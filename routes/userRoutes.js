const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

const userHandler = require("./handlers/user");

// TODO: Implement route controllers for user
router
  .get("/",userHandler.getAll, responseMiddleware)
  .get("/:id", userHandler.getOne, responseMiddleware)
  .post("/", createUserValid, userHandler.createUser, responseMiddleware)
  .put("/:id", updateUserValid, userHandler.updateUser, responseMiddleware)
  .delete("/:id", userHandler.deleteUser, responseMiddleware);

module.exports = router;
