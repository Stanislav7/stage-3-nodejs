const { fighter } = require("../models/fighter");
const Joi = require("@hapi/joi");

const createFighterSchema = Joi.object({
  name: Joi.string().alphanum().min(3).max(30).required(),
  health: Joi.number().integer().min(45).max(100).required(),
  power: Joi.number().integer().min(1).max(10).required(),
  defense: Joi.number().integer().min(1).max(10).required(),
});

const updateFighterSchema = Joi.object({
  name: Joi.string().alphanum().min(3).max(30),
  health: Joi.number().integer().min(45).max(100),
  power: Joi.number().integer().min(1).max(10),
  defense: Joi.number().integer().min(1).max(10),
});

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  if (req.body.id) {
    (res.error = { error: true, message: "Bad Request" }), next();
  }
  const fighterInfo = req.body;
  const vaidationResult = createFighterSchema.validate(fighterInfo).value;
  if (vaidationResult.error) {
    res.error = {
      error: true,
      message: "Fighter entity to create is not valid",
    };
    next();
  }
  const newFighter = {
    name: vaidationResult.name,
    health: vaidationResult.health,
    power: vaidationResult.power,
    defense: vaidationResult.defense,
  };
  res.newFighter = newFighter;
  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  if (req.body.Id) {
    (res.error = { error: true, message: "Bad Request" }), next();
  }
  const fighterInfo = req.body;
  const vaidationResult = updateFighterSchema.validate(fighterInfo).value;
  if (vaidationResult.error) {
    res.error = {
      error: true,
      message: "Fighter entity to create is not valid111111111111",
    };
    next();
  }
  const updatedFighter = {};
  if (vaidationResult.name) {
    updatedFighter.name = vaidationResult.name;
  }
  if (vaidationResult.health) {
    updatedFighter.health = vaidationResult.health;
  }
  if (vaidationResult.power) {
    updatedFighter.power = vaidationResult.power;
  }
  if (vaidationResult.defense) {
    updatedFighter.defense = vaidationResult.defense;
  }
  res.updatedFighter = updatedFighter;
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

/*
exports.fighter = {
    "id": "Ryuididididid",
    "name": "Ryu",
    "health": 45,
    "power": 0,
    "defense": 1,
}
*/