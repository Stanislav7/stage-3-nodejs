const { user } = require("../models/user");
const Joi = require("@hapi/joi");

const createUserSchema = Joi.object({
  firstName: Joi.string().alphanum().min(3).max(30).required(),
  lastName: Joi.string().alphanum().min(3).max(30).required(),
  email: Joi.string()
    .pattern(new RegExp("([a-zA-Z0-9]+[.|_|-]*)*@gmail.com$"))
    .required(),
  phoneNumber: Joi.string()
    .pattern(new RegExp("^(\\+380)[0-9]{9}$"))
    .required(),
  password: Joi.string().alphanum().min(3).max(30).required(),
});

const updateUserSchema = Joi.object({
  firstName: Joi.string().alphanum().min(3).max(30),
  lastName: Joi.string().alphanum().min(3).max(30),
  email: Joi.string().pattern(new RegExp("([a-zA-Z0-9]+[.|_|-]*)*@gmail.com$")),
  phoneNumber: Joi.string().pattern(new RegExp("^(\\+380)[0-9]{9}$")),
  password: Joi.string().alphanum().min(3).max(30),
});

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  if (req.body.id) {
    (res.error = { error: true, message: "Bad Request" }), next();
  }
  const userInfo = req.body;
  const vaidationResult = createUserSchema.validate(userInfo).value;
  if (vaidationResult.error) {
    res.error = {
      error: true,
      message: "User entity to create is not valid",
    };
    next();
  }
  const newUser = {
    firstName: vaidationResult.firstName,
    lastName: vaidationResult.lastName,
    email: vaidationResult.email,
    phoneNumber: vaidationResult.phoneNumber,
    password: vaidationResult.password,
  };
  res.newUser = newUser;
  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  if (req.body.Id) {
    (res.error = { error: true, message: "Bad Request" }), next();
  }
  const userInfo = req.body;
  const vaidationResult = updateUserSchema.validate(userInfo).value;
  if (vaidationResult.error) {
    res.error = {
      error: true,
      message: "User entity to create is not valid",
    };
    next();
  }
  const updatedUser = {};
  if (vaidationResult.firstName) {
    updatedUser.firstName = vaidationResult.firstName;
  }
  if (vaidationResult.lastName) {
    updatedUser.lastName = vaidationResult.lastName;
  }
  if (vaidationResult.email) {
    updatedUser.email = vaidationResult.email;
  }
  if (vaidationResult.phoneNumber) {
    updatedUser.phoneNumber = vaidationResult.phoneNumber;
  }
  if (vaidationResult.password) {
    updatedUser.password = vaidationResult.password;
  }
  res.updatedUser = updatedUser;
  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
