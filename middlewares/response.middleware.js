const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if (res.error) {
    const error = res.error;
    res.status(400).send(error);
    return;

  }
  if (res.data) {
    res.status(200).send(res.data);
    return;
 
  }
  res.status(500).send({
    error: true,
    message: "Internal Server Error",
  });
  return;

};

exports.responseMiddleware = responseMiddleware;
