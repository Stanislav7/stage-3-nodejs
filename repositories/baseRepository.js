const { dbAdapter } = require("../config/db");
const { v4 } = require("uuid");

class BaseRepository {
  constructor(collectionName) {
    this.dbContext = dbAdapter.get(collectionName);
    this.collectionName = collectionName;
  }

  generateId() {
    return v4();
  }

  getAll() {
    return this.dbContext.value();
  }

  getOne(search) {
    return this.dbContext.value().find((item) => item.id === search);
  }

  async create(data) {
    data.id = this.generateId();
    data.createdAt = new Date();
    const list = await this.dbContext.push(data).write();

    return list.find((it) => it.id === data.id);
  }

  async update(params) {
    const { id, dataToUpdate } = params;
    dataToUpdate.updatedAt = new Date();
    const elementToUpdate = this.dbContext
      .value()
      .find((item) => item.id === id);
    if (!elementToUpdate) return null;
    const updatedElement = Object.assign(elementToUpdate, dataToUpdate);
    this.dbContext.write();
    return this.dbContext.value().find((it) => it.id === id);
  }

  async delete(id) {
    const elementToDelete = this.dbContext
      .value()
      .find((item) => item.id === id);
    const index = this.dbContext.value().findIndex((item) => item.id === id);
    if (index === -1) return null;
    this.dbContext.value().splice(index, 1);
    this.dbContext.write();
    return await elementToDelete;
  }
}

exports.BaseRepository = BaseRepository;
