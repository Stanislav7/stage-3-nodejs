const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getOne(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  getAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }
  async create(fighter) {
    const item = await FighterRepository.create(fighter);
    if (!item) {
      return null;
    }
    return item;
  }
  async update(fighter) {
    const item = await FighterRepository.update(fighter);
    if (!item) {
      return null;
    }
    return item;
  }
  async delete(fighter) {
    const item = await FighterRepository.delete(fighter);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
