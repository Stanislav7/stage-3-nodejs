const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  getOne(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  getAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  async create(user) {
    const item = await UserRepository.create(user);
    if (!item) {
      return null;
    }
    return item;
  }
  async update(user) {
    const item = await UserRepository.update(user);
    if (!item) {
      return null;
    }
    return item;
  }
  async delete(user) {
    const item = await UserRepository.delete(user);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
